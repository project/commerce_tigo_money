<?php

namespace Drupal\commerce_tigo_money;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\TransferException;

class ApiClient {

    const API_BASE_TOKEN_URL = "https://prod.api.tigo.com/v1/";
    const SANDBOX_API_BASE_TOKEN_URL = "https://securesandbox.tigo.com/v1/";
    const API_BASE_PAYMENT_URL = "https://prod.api.tigo.com/v2/";
    const SANDBOX_API_BASE_PAYMENT_URL = "https://securesandbox.tigo.com/v2/";

    protected static $_sandbox = false;
    protected $public_key;
    protected $secret_key;
    public $agent_account;
    public $agent_pin;
    public $agent_name;

    public function __construct($client_id, $client_secret, $account, $pin, $name) {
        $this->public_key = $client_id;
        $this->secret_key = $client_secret;
        $this->agent_account = $account;
        $this->agent_pin = $pin;
        $this->agent_name = $name;
    }

    public function sandboxMode($status = false) {
        if ($status) {
            self::$_sandbox = true;
        }

    }

    public static function getBaseTokenURL() {
        if (self::$_sandbox) {
            return self::SANDBOX_API_BASE_TOKEN_URL;
        }

        return self::API_BASE_TOKEN_URL;
    }

    public static function getBasePaymentURL() {
        if (self::$_sandbox) {
            return self::SANDBOX_API_BASE_PAYMENT_URL;
        }

        return self::API_BASE_PAYMENT_URL;
    }

    public function client($payment = true) {
        return new GuzzleClient([
            'base_uri' => $payment ? self::getBasePaymentURL() : self::getBaseTokenURL(),
            'allow_redirects' => true,
        ]);
    }

    /**
     * @return mixed
     */
    public function getToken() {
        $logger = \Drupal::logger('commerce_tigo_money');
        $logger->notice('Api: Requesting token');
        try {
            $response = $this->client(false)->post("oauth/mfs/payments/tokens",
                [
                    "headers" => [
                        "Content-Type" => "application/x-www-form-urlencoded",
                        "Authorization" => ["Basic " . $this->encodeCredentials()],
                    ],
                    "json" => [
                        "grant_type" => "client_credentials",
                    ],
                ]);

            return self::responseJson($response);

        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting token. ' . $e->getMessage());
            return self::getErrorMessage($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function authorization(array $params) {
        $logger = \Drupal::logger('commerce_tigo_money');
        $logger->notice('Api: Requesting authorization');
        $params = array_merge(
            [
                'MasterMerchant' => [
                    'account' => $this->agent_account,
                    'pin' => $this->agent_pin,
                    'id' => $this->agent_name,
                ],
            ], $params);

        try {
            $response = $this->client()->post("tigo/mfs/payments/authorizations",
                [
                    "headers" => [
                        "Authorization" => "Bearer " . $this->getToken()->accessToken,
                        "Content-Type" => "application/json",
                    ],
                    "json" => $params,
                ]);

            return json_decode((string) $response->getBody(), true);

        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting authorization. ' . $e->getMessage());
            return self::getErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $mfsTransactionId
     * @param $merchantTransactionId
     * @return mixed
     */
    public function reverse($mfsTransactionId, $merchantTransactionId) {
        $logger = \Drupal::logger('commerce_tigo_money');
        $logger->notice('Api: Requesting reverse transaction');
        try {
            $response = $this->client()->delete("tigo/mfs/payments/transactions/PRY/$mfsTransactionId/$merchantTransactionId",
                [
                    "headers" => [
                        "Authorization" => "Bearer " . $this->getToken()->accessToken,
                        "Content-Type" => "application/json",
                    ],
                ]);
            return json_decode((string) $response->getBody(), true);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting reverse transaction. ' . $e->getMessage());
            return self::getErrorMessage($e->getMessage());
        }
    }

    /**
     * @param $mfsTransactionId
     * @param $merchantTransactionId
     * @return mixed
     */
    public function getTransaction($mfsTransactionId, $merchantTransactionId) {
        $logger = \Drupal::logger('commerce_tigo_money');
        $logger->notice('Api: Requesting transaction');
        try {
            $response = $this->client()->get("tigo/mfs/payments/transactions/PRY/$mfsTransactionId/$merchantTransactionId",
                [
                    "headers" => [
                        "Authorization" => "Bearer " . $this->getToken()->accessToken,
                        "Content-Type" => "application/json",
                    ],
                ]);
            return json_decode((string) $response->getBody(), true);
        } catch (TransferException $e) {
            $logger->error('Api: An error occurred while requesting transaction. ' . $e->getMessage());
            return self::getErrorMessage($e->getMessage());
        }
    }

    public function encodeCredentials() {
        $access = $this->public_key . ":" . $this->secret_key;
        return base64_encode($access);
    }

    public static function responseJson($response) {
        return \GuzzleHttp\json_decode(
            $response->getBody()->getContents()
        );
    }

    public static function getErrorMessage($response) {
        $pattern = "~{(.*?)}~";

        preg_match($pattern, $response, $matches);

        if (empty($matches)) {
            return $response;
        }

        $json = \GuzzleHttp\json_decode($matches[0]);

        return $json->error_description;
    }
}
