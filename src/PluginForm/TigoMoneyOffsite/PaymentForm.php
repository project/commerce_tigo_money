<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Drupal\commerce_tigo_money\PluginForm\TigoMoneyOffsite;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_tigo_money\ApiClient;
use Drupal\Core\Form\FormStateInterface;

class PaymentForm extends BasePaymentOffsiteForm {

    protected $integrityHash;

    /**
     *    * {@inheritdoc}
     * */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);
        $logger = \Drupal::logger('commerce_tigo_money');

        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = $this->entity;
        /** @var \Drupal\commerce_tigo_money\Plugin\Commerce\PaymentGateway\TigoMoneyPaymentGatewayInterface $plugin */
        $plugin = $payment->getPaymentGateway()->getPlugin();

        $public_key = $plugin->getPublicKey();
        $secret_key = $plugin->getSecretKey();
        $agent_account = $plugin->getAccount();
        $agent_pin = $plugin->getPin();
        $agent_name = $plugin->getStoreName();
        $gateway_mode = $plugin->getMode();

        $tigo_money = new ApiClient($public_key, $secret_key, $agent_account, $agent_pin, $agent_name);

        if ($gateway_mode == 'live') {
            $tigo_money->sandboxMode(false);
        } else {
            $tigo_money->sandboxMode(true);
        }

        // Payment data.
        $payment_amount = $payment->getAmount()->getNumber();
        $order_id = $payment->getOrderId();
        $currency_code = $payment->getAmount()->getCurrencyCode();
        $order = $payment->getOrder();
        // You must create a field in customer profile types field_phone_number.
        $phone = $order->getBillingProfile()->get('field_phone_number')->value;
        $email = $order->getEmail();
        $callback = $plugin->getNotifyUrl()->toString();

        $data = [
            'Subscriber' => [
                'account' => $phone,
                'countryCode' => '595',
                'country' => 'PRY',
                'emailId' => $email,
            ],
            'redirectUri' => $form['#return_url'],
            'callbackUri' => $callback,
            'language' => 'SPA',
            'OriginPayment' => [
                'amount' => $payment_amount,
                'currencyCode' => $currency_code,
                'tax' => '0.00',
                'fee' => '0.00',
            ],
            'exchangeRate' => '1',
            'LocalPayment' => [
                'amount' => $payment_amount,
                'currencyCode' => $currency_code,
            ],
            'merchantTransactionId' => $order_id,
        ];

        $transaction_data = $tigo_money->authorization($data);

        $data = [];

        if (isset($transaction_data['redirectUrl'])) {
            $redirect_url = $transaction_data['redirectUrl'];
            $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, BasePaymentOffsiteForm::REDIRECT_GET);
        } else {
            if (isset($transaction_data['error'])) {
                $logger->error('Payment: Error: ' . $transaction_data['error'] . '. Desc: ' . $transaction_data['error_description']);
            }
            \Drupal::messenger()->addWarning($this->t('There was a problem with your payment..'));

            $back = $this->t('Go back');
            $cancel = $form['#cancel_url'];
            $help_message = $this->t('Authorization error. Contact the site administrator.');
            $form['commerce_message'] = [
                '#markup' => '<div class="checkout-help">' . $help_message . ' <a href="' . $cancel . '">' . $back . '</a></div>',
                '#weight' => -10,
            ];
        }

        return $form;
    }
}