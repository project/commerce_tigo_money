<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\commerce_tigo_money\Plugin\Commerce\PaymentGateway;

/**
 *
 * @author Ruben Jara
 */
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;

interface TigoMoneyPaymentGatewayInterface extends SupportsAuthorizationsInterface, SupportsNotificationsInterface {

    /**
     * Get the configured Tigo Money API Public key.
     *
     * @return string
     *   The Tigo Money API Public key.
     */
    public function getPublicKey();

    /**
     * Get the configured Tigo Money API Secret key.
     *
     * @return string
     *   The Tigo Money API Secret key.
     */
    public function getSecretKey();

    /**
     * Get the configured Tigo Money Agent Store name.
     *
     * @return string
     *   The Tigo Money Agent Store name.
     */
    public function getStoreName();

    /**
     * Get the configured Tigo Money Agent Account.
     *
     * @return string
     *   The Tigo Money Agent Account.
     */
    public function getAccount();

    /**
     * Get the configured Tigo Money Agent PIN.
     *
     * @return string
     *   The Tigo Money Agent PIN.
     */
    public function getPin();

}
