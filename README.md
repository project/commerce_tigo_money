# Commerce Tigo Money
Allows merchants to accept payments via [Tigo Money mobile wallet](https://money.tigo.com.py/empresas/boton-de-pago) for Drupal 9/10. For developers see [api documentation](https://cdn.tigo.com.py/ard/2017/febrero/tigo-money-boton-pago/Documentacion_Developers_v_0_7.pdf).
